# vagrant-ansible-packer-elk

This is test environment to use it for testing  a migration from ELK 6 version to version 7

**Pre-built Vagrant Box**

Test environment for ELK Stack on Debian or CentOs

This example build configuration installs and configures Debian 10 amd64 minimal using Ansible, and then generates a Vagrant box file for VirtualBox.

The example can be modified to use more Ansible roles, plays, and included playbooks to fully configure (or partially) configure a box file suitable for deployment for development environments.

You can use Amazon ami builder too.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build the Vagrant box file:

 * [Packer](http://www.packer.io/)
 * [Vagrant](http://vagrantup.com/)
 * [VirtualBox](https://www.virtualbox.org/) (if you want to build the VirtualBox box)
 * [Ansible](http://docs.ansible.com/intro_installation.html)

## Usage

Make sure all the required software (listed above) is installed, then cd to the directory containing this README.md file, and run:

```s
packer build -var 'version=1.2.0' centos-7.json
```

After a few minutes, Packer should tell you the box was generated successfully, and the box can uploaded to Vagrant Cloud.

> **Note**: This configuration includes a post-processor that pushes the built box to Vagrant Cloud (which requires a `VAGRANT_CLOUD_TOKEN` environment variable to be set) by default it is commented out; decommented the `vagrant-cloud` post-processor from the Packer template to build the box locally and not push it to Vagrant Cloud is the default.
You don't need to specify a `version` variable either, if not using the `vagrant-cloud` post-processor.

```json
  "post-processors": [
    [
      {
        "output": "builds/{{.Provider}}--debian-10.box",
        "type": "vagrant"
      },
      {
        "type": "vagrant-cloud",
        "box_tag": "tobkern1980/debian10",
        "version": "{{user `version`}}"
      }
    ]
  ]
```

## Testing built boxes

There's an included Vagrantfile that allows quick testing of the built Vagrant boxes. From this same directory, run the following command after building the box:

```sh
vagrant up
```
